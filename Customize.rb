system("clear") || system("cls")

def press_to_exit(val)
  puts "(Press any key to exit)"
  gets
  exit val
end

# Customize ONLY here!
# Fill the THIRD COLUMN with your repo's data:
vals = [
  ["project's full name",         "{{PKG_NAME}}",         "Project Name"],
  ["project's slugified name",    "{{PKG_SLUG}}",         "project-name"],
  ["name or pseudonym",           "{{PKG_AUTHOR}}",       "Silent Fox"],
  ["email",                       "{{PKG_USEREMAIL}}",    "silentfox78642@gmail.com"],
  ["project's description",       "{{PKG_DESCRIPTION}}",  "Lorem ipsum dolor sit amet"]
]
# End customization

puts "#################################"
puts "# Template Customization Script #"
puts "#################################"
puts ""
puts "0. Have you edited this script to reflect your project? (Y/n)"
reply = gets.chomp.downcase
if reply == "n"
  puts "Then do so before running it."
  press_to_exit(1)
end

puts "1. Now, some checks:"
vals.each.with_index(1) do |v, i|
  puts "1.#{i}. Is <#{v[2]}> your #{v[0]}? (Y/n)"
  reply = gets.chomp.downcase
  if reply == "n"
    puts "Then edit this script before running it!"
    press_to_exit(1)
  end
end

puts "2. Begining the customization process:"
vals.each.with_index(1) do |v, i|
  files = Dir["./*"]
  files.each do |va|
    contents = File.read(va)
    contents.gsub!(Regexp.new(v[1]), v[2])
    File.open(va, 'w') do |file|
      file.write contents
    end
  end
  puts "  2.#{i}. Replaced all <#{v[1]}> instances with <#{v[2]}>."
end

puts "3. Do you want to delete this file? (Y/n)"
reply = gets.chomp.downcase
if reply != "n"
  File.delete("Customize.rb")
end

puts "5. Everything set up!"
press_to_exit(0)