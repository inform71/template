# {{PKG_NAME}}

[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC_BY--NC--SA_4.0-lightgrey.svg)][cc-license]
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)][contributor-covenant]

{{PKG_DESCRIPTION}}

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request

Please, also read our [Contributing Guidelines](CONTRIBUTING.md).

### Code of Conduct

Please note that this project is released with a [Contributor Code of Conduct](CODE-OF-CONDUCT.md).

By participating in this project you agree to abide by its terms.

## History & Versioning

See the [Changelog](CHANGELOG.md) for further history.

This project uses [SemVer][semver2] for versioning. For the versions
available, see the [tags on this repository][tags].

## License

[![Creative Commons License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-license].

[cc-license]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[tags]: https://gitlab.com/inform71/{{PKG_SLUG}}/-/tags
[semver2]: https://semver.org/spec/v2.0.0.html
[contributor-covenant]: https://www.contributor-covenant.org/version/2/1/code_of_conduct/
